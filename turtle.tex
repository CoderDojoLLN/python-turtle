\documentclass[11pt]{article}
\synctex=1
\usepackage{graphicx}
\usepackage[utf8x]{inputenc}
\usepackage[frenchb]{babel}
\usepackage[T1]{fontenc}
\usepackage[a4paper, top=2.5cm, bottom=2.5cm, inner=2cm, outer=5.5cm, marginparwidth=4cm, marginparsep=0.5cm]{geometry}
\usepackage[factor=1000]{microtype}     %awesome package to deal with text spacing, special char in margin...
\usepackage{lmodern}                    %best font for latex (latin modern is derived from computer modern)
\usepackage{amssymb,array}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{xspace}
\usepackage{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{upquote}
\usepackage{framed}
\usepackage{marginnote}
\usepackage{ragged2e}
\usepackage{hyperref}

\author{Kodo Louvain-la-Neuve}

\title{Apprentissage de Python avec le module \texttt{turtle}}
\date{Septembre 2017}

\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.7,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{deepgray}{rgb}{0.4,0.4,0.4}

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

\renewcommand{\familydefault}{\sfdefault}
\lstset{language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
showstringspaces=false            %
}

\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}

\newcommand{\trad}[1]{\marginnote{\textcolor{deepblue}{#1}}}

\usetikzlibrary{turtle}

\begin{document}

\maketitle

Pendant cette session,\trad{\RaggedRight Retrouve les traductions anglais~$\rightarrow$~français dans cette marge.}
nous allons découvrir le langage de programmation Python.
Guido van Rossum, le créateur de Python, a choisi de lui donner le nom de son émission préférée~:
Monty Python’s Flying Circus.
Python est un des langages de programmation les plus connus.
Il est utilisé par des grandes organisations telles que Wikipedia, Google et la NASA.
Beaucoup de personnes aiment utiliser Python parce que c’est un langage facile à lire.
Python est un langage vivant.
Au cours des évolutions des fonctions sont ajoutées ou modifiées.
Il arrive également que la grammaire soit modifée.

\section{Installation de Python}

Si Python n'est pas déjà installé sur ton ordinateur, tu peux le télécharger sur \url{https://python.org/downloads/}.
Télécharge la dernière version de \textbf{Python3}.

\section{Découverte de l'éditeur de code}

Une fois Python installé sur ton ordinateur, tu peux lancer l'éditeur de code.

\begin{description}
    \item[Sous Windows,] cherche IDLE dans le menu Démarrer.
    \item[Sous Mac OS X,] ouvre Terminal.app, tape \texttt{idle3} et appuie sur la touche Entrée.
    \item[Sous Linux,] ouvre un Terminal, tape \texttt{idle3} et appuie sur la touche Entrée.
\end{description}

Une fois l'IDLE démarré, tu as accès à une fenêtre appelée \texttt{Python Shell}.
C'est dans cette fenêtre que le code est exécuté.
Afin d'exécuter le code, nous allons l'écrire dans un fichier.
Pour cela, crée un fichier vide en cliquant sur \texttt{File} $\rightarrow$ \texttt{New File}.
Une nouvelle fenêtre s'ouvrira, avec  \texttt{Untitled} dans la barre de titre.
Tu pourras ensuite sauvegarder ce fichier vide en cliquant sur \texttt{File} $\rightarrow$ \texttt{Save}.

\begin{framed}
Petite astuce~:~pour chaque étape de cette session, utilise un fichier différent.
Ça te permettra de garder une trace de ce que tu as fait
et c'est pratique pour se souvenir de comment tu as fait les choses~!
\end{framed}

\section{Hello World}

\begin{enumerate}
    \item Si tu ne l'as pas déjà fait, ouvre IDLE et ouvre un fichier vide.

    \item Tape le code suivant dans ton nouveau fichier :
\begin{lstlisting}
print("Hello World !")
\end{lstlisting}

    \item Enregistre ton code.
    Tu peux sauvegarder ce premier fichier sur ton bureau et l'appeler \texttt{hello.py}.

    \item Clique ensuite sur \texttt{Run} $\rightarrow$ \texttt{Run Module} pour lancer ton programme.
\end{enumerate}

Que se passe-t-il~? N'oublie pas que ton programme s'exécute dans la fenêtre \texttt{Python Shell}.

Félicitations~! Tu viens d'écrire ton premier programme en Python.
Tu peux modifier la ligne de code pour faire dire ce que tu veux à ton programme.

\begin{framed}
Sais-tu qu'écrire \texttt{Hello World} est traditionnellement la première chose qu'on apprend
quand on commence avec un nouveau langage de programmation~?
Il y a même une page wikipedia avec ce petit programme dans près de 200 langages~:
\url{https://fr.wikipedia.org/wiki/Liste_de_programmes_Hello_world}.
\end{framed}

\section{Bonjour Tortue}

Maintenant, jouons avec des tortues.

Une tortue est un petit robot qui dessine sur ton écran.
On peut lui donner des instructions pour la déplacer en utilisant des commandes en Python.

\begin{enumerate}
    \item Ouvre un nouveau fichier et écrit le code suivant~:
          \marginnote{\textcolor{deepblue}{turtle~=~tortue}}[1.8em]
          \marginnote{\textcolor{deepblue}{forward~=~en avant}}[5.5em]
\begin{lstlisting}
import turtle

tortue = turtle.Turtle()
tortue.forward(100)
\end{lstlisting}

    \item Enregistre ton programme dans le fichier \texttt{myturtle.py}
          et clique sur \texttt{Run} $\rightarrow$ \texttt{Run Module}.

          Un fenêtre s'est-elle ouverte~?
          La tortue a-t-elle avancé de 100 pixels~?

    \item Il existe d'autres commandes.

          Utilise \lstinline{backward(distance)}\trad{backward~=~en arrière\\left~=~gauche\\right~=~droite},
          \lstinline{right(angle)} et \lstinline{left(angle)}.

          Par exemple, \lstinline{backward(20)} dit à la tortue de reculer de 20 pixels
          et \lstinline{right(90)} dit à la tortue de tourner vers la droite de 90 degrés.
          Tu peux enchainer plusieurs instructions, elles seront exécutées dans l’ordre.

    \item Peux-tu prédire le déplacement de la tortue en lisant simplement ce code~?

\begin{lstlisting}
import turtle

tortue = turtle.Turtle()

tortue.speed('slow')
tortue.shape('turtle')

tortue.forward(100)
tortue.right(120)
tortue.forward(100)
tortue.left(90)
tortue.backward(100)
tortue.left(90)
tortue.forward(50)
\end{lstlisting}

    \item Recopie le code et vérifie ta prédiction.
\end{enumerate}

\section{Angles et degrés}

Comme tu as pu le constater,
\lstinline{forward} et \lstinline{backward} déplacent la tortue d'un certain nombre de pixels.
Par contre, \lstinline{left} et \lstinline{right} tournent la tortue d'un certain angle.
Observons une tortue qui tourne vers la gauche lorsqu'elle avance vers l'Est.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm,%
                    bcr/.style = {only marks, mark=*}]
    \draw[thick] (0, -2) -- (0, 2);
    \draw[thick] (-2, 0) -- (2, 0);
    \node[anchor=south] at (0, 2) {Nord};
    \node[anchor=north] at (0, -2) {Sud};
    \node[anchor=east] at (-2, 0) {Ouest};
    \node[anchor=west] at (2, 0) {Est};

    \fill (0,0) ellipse (2.5mm and 2mm);
    \fill (0.30, 0) circle (0.75mm);
    \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
    \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
    \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
    \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;

    \draw[->, thick, deepred] (12.5mm, 0mm) arc (0:90:12.5mm);
    \node[right, deepred] at (1,1) {\lstinline{left(90)}};
    \draw[->, thick, deepblue] (10mm, 0mm) arc (0:180:10mm);
    \node[left, deepblue] at (-0.8,0.666) {\lstinline{left(180)}};
    \draw[->, thick, deepgreen] (7.5mm, 0mm) arc (0:270:7.5mm);
    \node[left, deepgreen] at (-0.5,-0.5) {\lstinline{left(270)}};
    \draw[->, thick, deepgray] (5mm, 0mm) arc (0:360:5mm);
    \node[right, deepgray] at (0.4,-0.4) {\lstinline{left(360)}};

\end{tikzpicture}
\end{center}

En faisant tourner la tortue de \textcolor{deepred}{90 degrés vers la gauche}, elle sera orientée vers le Nord.
Si tu la fais tourner de \textcolor{deepblue}{180 degrés}, elle s'orientera vers l'Ouest.
En la faisant tourner de \textcolor{deepgreen}{270 degrés vers la gauche}, elle s'orientera vers le Sud.
Si tu la fais tourner de \textcolor{deepgray}{360 degrés}, elle reste dans la même direction.

Que se passe-t-il quand on tourne vers la droite~?

\section{À quoi sert le code au tout début de ton programme ?}

\begin{itemize}
    \item \lstinline{import turtle} indique à Python qu'il faut importer en mémoire la bibliothèque \lstinline{turtle}.
          Cette bibliothèque contient un ensemble de code que nous pouvons ensuite utiliser pour dessiner sur l’écran.
          Utiliser une bibliothèque nous permet de réutiliser du code créé par d'autres personnes et donc de gagner du temps.

    \item \lstinline{tortue = turtle.Turtle()} créé une tortue et la sauve sous le nom \lstinline{tortue}.
          En pratique, c'est la fonction \lstinline{Turtle} du module \lstinline{turtle} qui crée la tortue.
          Si l'on veut, on peut créer plusieurs tortues avec des noms différents.

    \item \lstinline{tortue.speed('slow')} \trad{speed~=~vitesse} change la vitesse
          à laquelle \lstinline{tortue} se déplace.
          Cette instruction prend une valeur parmi
          \begin{itemize}
                \item \lstinline{'slowest'}\trad{slowest~=~la plus lente}~;
                \item \lstinline{'slow'}\trad{slow~=~lente}~;
                \item \lstinline{'normal'}~;
                \item \lstinline{'fast'}\trad{fast~=~rapide}~;
                \item \lstinline{'fastest'}\trad{fastest~=~la plus rapide}~;
          \end{itemize}

    \item \lstinline{tortue.shape('turtle')}\trad{shape~=~forme} change la forme de \lstinline{tortue}.
          Les différentes formes possibles sont
          \begin{itemize}
                \item \lstinline{'classic'}, la forme par défaut;
                \item \lstinline{'arrow'}\trad{arrow~=~flèche}~;
                \item \lstinline{'circle'}\trad{circle~=~cercle}~;
                \item \lstinline{'square'}\trad{square~=~carré}~;
                \item \lstinline{'triangle'}~;
                \item \lstinline{'turtle'}~;
          \end{itemize}

\end{itemize}

Si tu veux, tu peux essayer de changer la forme de ta tortue ou la faire aller plus ou moins vite.

\section{Dessiner des formes}

Essaye de dessiner un carré en disant à la tortue comment se déplacer.

\begin{enumerate}

    \item Ouvre un nouveau fichier dans IDLE, et tapes-y ton code pour dessiner un carré.
    Essaye de terminer dans ta position de départ (même endroit, même sens).

    \item Enregistre ton programme et clique sur \texttt{Run} $\rightarrow$ \texttt{Run Module}.

    Est-ce que tu vois le carré ?

    En tournant quatre fois de 90 degrés, ta tortue tourne de 360 degrés au total.

    \item Et si tu essayais de faire un triangle~?

    Un triangle a trois angles, donc il faut faire tourner la tortue trois fois.
    Si tu veux que la tortue finisse à sa position de départ, tu dois la faire tourner de 360 degrés, comme pour le carré.
    Donc nous allons tourner de 120 degrés, puis encore de 120 degrés, et une fois de plus.

    Change ton code pour dessiner un triangle.
\end{enumerate}

\section{Choisir une couleur}

Quelle est ta couleur préférée?

Tu peux changer la couleur des lignes en utilisant \lstinline{pencolor()}\trad{pen~=~stylo\\color~=~couleur\\red~=~rouge}.
Par exemple, \lstinline{tortue.pencolor('red')} permet de dessiner en rouge.

Les couleurs peuvent être définies par des noms anglais
(une liste de noms reconnus se trouve sur \url{http://wiki.tcl.tk/37701}).
Mais tu peux également créer ta propre couleur en donnant les quantités de rouge, vert et bleu.
Par exemple, on peut utiliser \lstinline{tortue.pencolor((0.55, 0.71, 0.86))} pour dessiner en bleu pastel.
Fais attention à donner des valeurs de couleur comprises entre \lstinline{0.0} et \lstinline{1.0}.

Modifie le code de l’exercice précédent pour changer les couleurs de chacune des arrêtes du triangle.

Tu peux également changer la taille du crayon en utilisant \lstinline{pensize()}\trad{size~=~taille}.

\newpage

\section{Challenge}

Tu maîtrises maintenant les bases. Peux-tu dessiner cette maison sans lever le crayon et sans passer 2 fois sur la même ligne?

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm,%
                    bcr/.style = {only marks, mark=*}]
    \filldraw[very thick, red, draw=blue] (0, 0) -- (4, 0) -- (2, 2.8284) -- (0,0);
    \draw[very thick, green] (0,0) -- (0, -4) -- (4, -4) -- (4, 0) -- (0, -4) -- (0, 0) -- (4, -4);


\end{tikzpicture}
\end{center}

\section{Répéter un morceau de code (avec une boucle \textit{for})}

Dans le dernier programme que tu as écrit, tu as répété les mêmes commandes encore et encore.
Au lieu de toutes les écrire plusieurs fois, tu peux demander à l’ordinateur de les répéter pour toi.

Tu as peut-être déjà utilisé les boucles avec scratch ou blokkly.
Pour faire des boucles, tu as utilisé des blocs \texttt{Répéter indéfiniment},
\texttt{Répéter x fois} et \texttt{Répéter jusqu’à}.

En Python, les boucles \lstinline{for}\trad{for~=~pour} sont utilisées
pour répéter un morceau de code plusieurs fois de suite.
Dans le cas du carré, nous souhaitons répéter le code quatre fois (parce qu’un carré a 4 côtés).

\begin{enumerate}

    \item Ouvre un nouveau fichier et tape le code suivant à l’intérieur~:
    \marginnote{\textcolor{deepblue}{in~=~dans\\range~=~interval}}[10.5em]

\begin{lstlisting}
import turtle

tortue = turtle.Turtle()

tortue.speed('slow')
tortue.shape('turtle')

for i in range(4):
    tortue.forward(100)
    tortue.right(90)
\end{lstlisting}

    \item Enregistre ton programme et clique sur: \texttt{Run} $\rightarrow$ \texttt{Run module}.

    Tu peux remarquer que le programme est indenté (ou décalé vers la droite) sous la boucle \lstinline{for}.
    Python se sert des retraits afin de savoir quelles commandes il doit répéter.
    Tu peux utiliser la touche \texttt{Tabulation} pour ajouter des tabulations,
    et le raccourci \texttt{Majuscule-Tabulation} pour en enlever.

    \item Essaye de voir ce qu’il se passe si tu n'indentes pas la dernière ligne.
    Que fait ce programme~? Essaye de l’exécuter.

    \item Corrige l'indentation pour revenir à la version de ton programme qui dessine un carré.

    \item Tu vas maintenant modifier ton code pour utiliser des variables.
    Utiliser des variables te permet de rendre ton programme plus facile à modifier et à comprendre.

    Change ton code pour qu’il corresponde à ceci:

\begin{lstlisting}
import turtle

tortue = turtle.Turtle()

tortue.speed('slow')
tortue.shape('turtle')

cotes = 4
longueur = 100
angle = 90
for i in range(cotes):
    tortue.forward(longueur)
    tortue.right(angle)
\end{lstlisting}

    \item Enregistre ton programme et clique sur \texttt{Run} $\rightarrow$ \texttt{Run module}.

    Obtiens-tu le résulat voulu~?
    Modifie les valeurs des variables et observe le résultat.
\end{enumerate}

\section{Ton défi: dessiner d'autres formes}

Est-ce que tu peux dessiner les formes suivantes en changeant
les valeurs de \lstinline{longueur} et \lstinline{cotes}~?

\begin{itemize}
    \item un triangle? (trois côtés)
    \item un pentagone? (cinq côtés)
    \item un hexagone? (six côtés)
    \item un octogone? (huit côtés)
\end{itemize}

Au lieu de devoir calculer l’angle toi-même, pourquoi ne pas demander à l’ordinateur de le faire~?
Python est capable de faire certaines opérations sur des nombres,
comme les additions (\lstinline{+}), soustractions (\lstinline{-}),
multiplications (\lstinline{*}) et divisions (\lstinline{/}).

\newpage

\section{Plus difficile: dessiner une spirale}

Essaye de dessiner une spirale carrée.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm]

    \draw [very thick,blue][turtle=home]
    \foreach \i in {1,...,32}
    {
        [turtle={forward=0.125*\i,left}]
    };

    \begin{scope}[xshift=2cm, yshift=-2cm]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

\end{tikzpicture}
\end{center}

Plus difficile, dessine une véritable spirale.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm]
    \draw [very thick,blue][turtle=home]
    \foreach \i in {1,...,249}
    {
        [turtle={forward=0.0025*\i,left=11.25}]
    };

    \begin{scope}[xshift=0.3cm, yshift=-3.2cm]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

\end{tikzpicture}
\end{center}

\section{Grouper du code dans une fonction}

Il est possible de regrouper plusieurs instructions dans une fonction.
Lorsque l'on exécute une fonction, toutes les instructions sont répétées.

Nous allons utiliser une fonction pour dessiner facilement 2 carrés côte à côte.

\begin{enumerate}

    \item Ouvre un nouveau fichier et tape le code suivant à l’intérieur~:

\begin{lstlisting}
import turtle

tortue = turtle.Turtle()

tortue.speed('slow')
tortue.shape('turtle')

def carre():
    cotes = 4
    longueur = 100
    angle = 90
    for i in range(cotes):
        tortue.forward(longueur)
        tortue.right(angle)

carre()
tortue.forward(100)
carre()
\end{lstlisting}

    Pour définir une fonction, on utilise le mot-clé \lstinline{def} suivi d'un nom qu'on veut donner à la fonction.
    Tu peux remarquer que le contenu de la fonction est indenté comme pour la boucle \lstinline{for}.

    Pour réaliser les instructions de ta fonction, il te suffit de taper son nom suivi des parenthèses: \lstinline{carre()}

    \item Enregistre ton programme et clique sur: \texttt{Run} $\rightarrow$ \texttt{Run module}.

    Est-ce que les deux carrés sont bien dessinés~?

    \item Et si on changeait la taille du deuxième carré.

    Adapte ton code pour le faire correspondre à celui-ci.

\begin{lstlisting}
import turtle

tortue = turtle.Turtle()

tortue.speed('slow')
tortue.shape('turtle')

def carre(longueur):
    cotes = 4
    angle = 90
    for i in range(cotes):
        tortue.forward(longueur)
        tortue.right(angle)

carre(100)
tortue.forward(100)
carre(50)
\end{lstlisting}

    Les fonctions peuvent prendre un ou plusieurs arguments.
    Dans ce cas-ci, c'est la longueur du carré qui est utilisée comme argument.

    \item Enregistre ton programme et clique sur: \texttt{Run} $\rightarrow$ \texttt{Run module}.

    Est-ce que les deux carrés sont bien dessinés~? Le deuxième carré est-il plus petit que le premier~?

    \item Peux-tu modifier ta fonction pour utiliser cotes et
    longueur comme argument afin de dessiner toutes les formes à partir de ta fonction~?
\end{enumerate}

\section{Forme pleine}

    La tortue peut remplir tes formes avec une couleur.
    Pour cela, il faut commencer par définir la couleur désirée en utilisant \lstinline{tortue.fillcolor('red')}.
    \trad{fill~=~remplir\\begin~=~commencer\\end~=~terminer}
    Ensuite, il faut utiliser \lstinline{tortue.begin_fill()} avant de dessiner la forme à remplir
    et \lstinline{tortue.end_fill()} quand la forme est finie.
    Essaye de remplir ton carré de la couleur de ton choix.

\section{Quelques astuces}
    Si tu veux faire bouger la tortue sans laisser de marque derrière elle,
    tu peux utiliser \lstinline{tortue.penup()}\trad{up~=~haut\\down~=~bas}
    et \lstinline{tortue.pendown()} pour activer ou désactiver le crayon.
    Essaye de dessiner une ligne de tirets.

    La fonction \lstinline{tortue.home()}\trad{home~=~domicile} ramène la tortue au centre du canevas.

    La fonction \lstinline{tortue.goto(x,y)}\trad{goto~=~aller à} déplace la tortue à la coordonnée (x, y) du canevas.

    La fonction \lstinline{tortue.clear()}\trad{clear~=~nettoyer} efface l’écran.

    La fonction \lstinline{tortue.reset()}\trad{reset~=~réinitialiser} ramène la tortue au centre du canevas et efface l’écran.

\section{Ajoutons du hasard}
Dans Python, on peut générer des nombres de manière aléatoire (par hasard).
Pour cela, il faut importer le module \lstinline{random} au début du fichier.
Ensuite, on peut utiliser \lstinline{x = random.randint(1, 100)}\trad{random~=~au hasard} pour donner une valeur aléatoire entre 1 et 100 à \lstinline{x}.
On peut donc maintenant déplacer notre tortue de manière aléatoire en la faisant avancer ou tourner à partir de nombres choisis au hasard.

Voici un petit programme qui déplace la tortue aléatoirement 10 fois et qui dessine un carré après chaque déplacement.

\begin{lstlisting}
import turtle
import random

tortue = turtle.Turtle()

tortue.speed('fast')
tortue.shape('turtle')

def carre(longueur):
    cotes = 4
    angle = 90
    for i in range(cotes):
        tortue.forward(longueur)
        tortue.right(angle)

for i in range(10):
    x = random.randint(1, 200)
    y = random.randint(1, 200)
    tortue.goto(x,y)
    carre(100)
\end{lstlisting}

Peux-tu modifier le code pour:
\begin{itemize}
    \item ne dessiner que les carrés?
    \item dessiner des carrés de tailles aléatoires?
\end{itemize}

\section{La course des tortues}

C'est l'heure de la course des tortues.

\begin{enumerate}
    \item Crée 4 tortues.
    \item Une tortue est l'arbitre: elle trace la ligne de départ et la ligne d'arrivée puis se place à côté de la ligne d'arrivée.
    \item Les 3 autres tortues se placent côte-à-côte sur la ligne de départ prêtes au départ.
    \item Les tortues démarrent. À chaque étape, les tortues avancent d'une distance aléatoire.
    \item Quand une des tortues dépasse la ligne d'arrivée, la course s'arrête. Elle est déclarée vainqueuse.
\end{enumerate}

Voici un petit aperçu de la position des tortues avant le départ.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm]

    \draw[very thick] (0.5,0.5) -- (0.5, -2.5);
    \draw[very thick] (10,0.5) -- (10, -2.5);

    \begin{scope}[xshift=0cm, yshift=0cm, fill=red]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

    \begin{scope}[xshift=0cm, yshift=-1cm, fill=blue]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

    \begin{scope}[xshift=0cm, yshift=-2cm, fill=green!50!black]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

    \begin{scope}[xshift=10cm, yshift=1cm, rotate=-90]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

\end{tikzpicture}
\end{center}

À toi de jouer! Pose tes questions aux mentors si tu es bloqué.

\section{Lâche-toi}

Assemble différentes formes et vois ce que tu peux dessiner.

Est-ce que tu peux dessiner un oiseau~? Un serpent~? Un chat~? Un chien~? Un lion~? Un robot~?
Peux-tu écrire ton nom~?
Peux tu dessiner des formes géométriques?

Voici un exemple de forme à dessiner.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm]
    
    \draw [very thick,blue][turtle=home]
    \foreach \i in {1,...,90}
    {
        [turtle={forward=5,left=92}]
    };

    \begin{scope}[xshift=0cm, yshift=0cm]
        \fill (0,0) ellipse (2.5mm and 2mm);
        \fill (0.30, 0) circle (0.75mm);
        \fill (0,0) -- (0.24,0.19) -- (0.18, 0.23) -- cycle ;
        \fill (0,0) -- (-0.24,-0.19) -- (-0.18, -0.23) -- cycle ;
        \fill (0,0) -- (-0.24,0.19) -- (-0.18, 0.23) -- cycle ;
        \fill (0,0) -- (0.24,-0.19) -- (0.18, -0.23) -- cycle ;
    \end{scope}

\end{tikzpicture}
\end{center}

\end{document}

